#!/usr/bin/env python3

import arrow
import requests

def get_date():
    utc = arrow.utcnow()
    today = utc.format('YYYY-MM-DD')
    return today

class Generic:

    def __init__(self, blob):
        blob_type = type(blob)
        if blob_type == str:
            # need to revisit this later
            # this seems to be a problem within linescore
            print(f"str: {blob}")
            pass 
        else:
            for key, value in blob.items():
                if type(value) is not dict:
                    setattr(self, key, value)
                else:
                    #print(f"key: {key}")
                    setattr(self, key, Generic(value)) 
class Schedule:
    
    # http://statsapi.web.nhl.com/api/v1/schedule?date=2018-01-09

    def __init__(self,date=get_date()):
        self.date = date 
        schedule_url = f"https://statsapi.web.nhl.com/api/v1/schedule?date={self.date}"
        response = requests.get(schedule_url).json()
        self.games = []
        if response['totalGames'] >= 1:
           # we have games, proceed
           games_blob = response['dates'][0]['games']
           for game_data in games_blob:
                self.games.append(Schedule.Game(game_data)) 
          
            
    class Game:
        """
        TODO: needs content teams and status sub-classes
        """
        def __init__(self, blob):
            for key, value in blob.items():
                if type(value) == dict:
                    # dict
                    #print(f"SUB: {key}")
                    if key == "status":
                        self.status = Schedule.Game.GameStatus(value)
                    pass
                else:
                    setattr(self, key, value)

        class GameStatus:

            def __init__(self, blob):
                for key, value in blob.items():
                    setattr(self, key, value)
class Team:
    
    # https://statsapi.web.nhl.com/api/v1/teams/15

    def __init__(self, tid):
        self.team_id = tid
        self.team_url = f"https://statsapi.web.nhl.com/api/v1/teams/{self.team_id}?expand=team.roster"
        response = requests.get(self.team_url).json()
        team_data = response['teams'][0]
        for key,value in team_data.items():
            if type(value) is dict:
                # sub work
                #print(f"SUB: {key}")
                if key == 'venue':
                    self.venue = self.Venue(value)
                if key == 'roster':
                    self.roster = self.Roster(self.team_id)
            else:
                setattr(Team, key, value) 
        # build other classes

        self.stats = self.TeamStats(self.team_id)

    class Venue:
        def __init__(self, venue_blob):
            for key,value in venue_blob.items():
                setattr(self, key, value) 
                
    class Roster:

        # https://statsapi.web.nhl.com/api/v1/teams/15?expand=team.roster

        def __init__(self, team_id):
            url = f"https://statsapi.web.nhl.com/api/v1/teams/{team_id}/roster"
            response = requests.get(url).json()
            players = []
            for player in response['roster']:
                player_object = Team.Roster.People(player['person'])
                players.append(player_object)
            setattr(self, 'players', players)

        class People:

            def __init__(self, player_blob):
                for key, value in player_blob.items():
                    setattr(self, key, value)
                self.stats = self.PlayerStats(self.id)

            class PlayerStats:

                def __init__(self, player_id):
                    #print(f"getting stats for {player_id}")
                    stats_url = f"https://statsapi.web.nhl.com/api/v1/people/{player_id}/stats?stats=statsSingleSeason"
                    response = requests.get(stats_url).json()
                    #stats_object = response['stats'][0]['splits'][0]['stat']
                    stats_object = response['stats'][0]['splits']
                    if stats_object == []:
                        """
                        TODO: write something to pull the field names and populate empty players to keep results
                        consitent in format
                        """
                        pass
                    else:
                        stats_object = response['stats'][0]['splits'][0]['stat']
                        for key, value in stats_object.items():
                            setattr(self, key, value) 
    class TeamStats():
    
        # https://statsapi.web.nhl.com/api/v1/teams/15?expand=team.stats

        def __init__(self, team_id):
            url = f"https://statsapi.web.nhl.com/api/v1/teams/{team_id}?expand=team.stats"
            response = requests.get(url).json()
            team_stats = response['teams'][0]['teamStats'][0]['splits'][0]['stat']
            for key,value in team_stats.items():
                setattr(self, key, value) 

"""
Question: how to properly implement the Conferences and Divisions
series of endpoints
"""


class GameDetails:

    # http://statsapi.web.nhl.com/api/v1/game/2019021050/boxscore

    def __init__(self,game_id):
        # build sub-classes
        base_url = f"https://statsapi.web.nhl.com/api/v1/game/{game_id}"
        #self.boxscore = self.Boxscore(gameId) 
        #self.linescore = self.Linescore(gameId)
        #self.live = self.Live(gameId)
        self.boxscore = GameDetails.Boxscore(base_url)
        self.linescore = GameDetails.Linescore(base_url)
        self.live = GameDetails.Live(base_url)
    class Boxscore():

        def __init__(self,url):
            self.base_url = url
            self.teams = []
            full_url = f"{self.base_url}/boxscore"
            response = requests.get(full_url).json()
            for item in response['teams']:
                self.teams.append(Generic(response['teams'][item])) 
    class Linescore():

        def __init__(self,url):
            self.base_url = url
            self.periods = []
            self.teams = []

            full_url = f"{self.base_url}/linescore"

            response = requests.get(full_url).json()
            for item in response['periods']:
                self.periods.append(Generic(item)) 

            for item in response['teams']:
                self.teams.append(Generic(item))
    class Live():
        
        def __init__(self,url):
            self.base_url = url
            full_url = f"{self.base_url}/feed/live"
            response = requests.get(full_url).json()
            for key, value in response.items():
                setattr(self, key, value) 


class Tournaments:

    def __init__(self):
        # build sub-classes
        self.playoffs = self.Playoffs()

    class Playoffs():
        # default to current season unless a seasonId is specified 
        def __init__(self, seasonId=None):
            self.name = "Playoffs"
            self.id = seasonId
            if self.id is None:
                url = f"https://statsapi.web.nhl.com/api/v1/tournaments/playoffs?expand=round.series,schedule.game.seriesSummary"
            else:
                url = f"https://statsapi.web.nhl.com/api/v1/tournaments/playoffs?expand=round.series,schedule.game.seriesSummary&season={self.id}"
            response = requests.get(url).json()
            for key, value in response.items():
                print(key, value)
if __name__ == "__main__":
    """

    # perhaps I need to write code to detect if there are layers below the current one in a json object?

    t = Team(15)
    gid = '2022020478'
    s = Schedule()
    deets = GameDetails(gid)
    """
    #blah = Tournaments.Playoffs(20172018)
    blah = Tournaments.Playoffs()
    print(blah) 



    """
    print(deets.boxscore.teams[1].team.name)
    print(deets.boxscore.teams[0].coaches[0]['person'])
    print(deets.linescore.periods[0].away.goals)
    print(deets.linescore.periods[2].away.goals)
    """
    #print(gameid)
    #deets = GameDetails(gameid)
    # id 0 is away, 1 is home
    #print(deets.boxscore.teams[1].team.name)
    #print(deets.boxscore.teams[0].coaches[0]['person'])
    #print(deets.linescore.periods[0].away.goals)
